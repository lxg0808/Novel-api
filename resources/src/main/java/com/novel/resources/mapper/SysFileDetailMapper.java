package com.novel.resources.mapper;

import com.novel.resources.domain.SysFileDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 文件记录表 数据层
 *
 * @author novel
 * @since 2023-08-16 19:10:12
 */
@Mapper
public interface SysFileDetailMapper {
    /**
     * 根据id查询
     *
     * @param id 文件id
     * @return 文件记录表信息
     */
    SysFileDetail selectSysFileDetailById(String id);

    /**
     * 根据条件分页查询文件记录表
     *
     * @param sysFileDetail 文件记录表
     * @return 文件记录表集合信息
     */
    List<SysFileDetail> selectSysFileDetailList(SysFileDetail sysFileDetail);

    /**
     * 根据条件分页查询文件记录表
     *
     * @param sysFileDetail 文件记录表
     * @return 文件记录表集合信息
     */
    SysFileDetail selectSysFileDetail(SysFileDetail sysFileDetail);

    /**
     * 修改文件记录表
     *
     * @param sysFileDetail 文件记录表
     * @return 影响的行数
     */
    int updateSysFileDetailById(SysFileDetail sysFileDetail);

    /**
     * 新增文件记录表
     *
     * @param sysFileDetail 文件记录表
     * @return 影响的行数
     */
    int insertSysFileDetail(SysFileDetail sysFileDetail);

    /**
     * 根据主键删除文件记录表
     *
     * @param id 文件id
     * @return 影响的行数
     */
    int deleteSysFileDetailById(String id);

    /**
     * 批量删除文件记录表
     *
     * @param ids 需要删除的文件id集合
     * @return 影响的行数
     */
    int batchDeleteSysFileDetailByIds(String[] ids);

    int deleteSysFileDetail(SysFileDetail sysFileDetail);
}