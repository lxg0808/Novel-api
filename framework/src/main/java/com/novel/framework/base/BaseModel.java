package com.novel.framework.base;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * model基类
 *
 * @author novel
 * @since 2019/4/16
 */
@Data
public class BaseModel implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * Id
     */
    private Long id;

    /**
     * 创建者
     */
    @Excel(name = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", format = "yyyy-MM-dd HH:mm:ss", width = 30)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新者
     */
    @Excel(name = "更新者")
    private String updateBy;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", format = "yyyy-MM-dd HH:mm:ss", width = 30)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 备注
     */
    @Excel(name = "备注", width = 100)
    private String remark;
}
