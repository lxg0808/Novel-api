package com.novel.framework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 白名单配置类
 *
 * @author novel
 * @since 2023/1/10 20:17
 */
@ConfigurationProperties(prefix = "sa-token")
@Component
@Data
public class SaTokenProperties {
    /**
     * 白名单
     */
    List<String> whiteList = new ArrayList<>();

    public SaTokenProperties() {
        whiteList.add("/login/**");
        whiteList.add("/common/**");
        whiteList.add("/resources/**");
        whiteList.add("/websocket/**");
        whiteList.add("/swagger**/**");
        whiteList.add("/maku-generator/**");
    }

    public void setWhiteList(List<String> whiteList) {
        whiteList.add("/login/**");
        whiteList.add("/common/**");
        whiteList.add("/resources/**");
        whiteList.add("/websocket/**");
        whiteList.add("/swagger**/**");
        whiteList.add("/maku-generator/**");
        this.whiteList = whiteList;
    }
}