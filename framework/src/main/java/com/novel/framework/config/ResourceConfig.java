package com.novel.framework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 资源访问配置
 *
 * @author novel
 * @since 2019/6/4
 */
@ConfigurationProperties(prefix = ResourceConfig.RESOURCE_PREFIX)
@Data
@Component
public class ResourceConfig {
    public static final String RESOURCE_PREFIX = "spring.file-storage";

    /**
     * 是否开启文件服务器存储文件
     */
    private boolean enable = false;
    /**
     * 是否开启缓存
     */
    private boolean cacheEnable = false;
}
