package com.novel.framework.satoken.service;

/**
 * 登录服务
 *
 * @author novel
 * @since 2023/1/10 18:39
 */
public interface SysLoginService {


    /**
     * 登录验证
     *
     * @param username   用户名
     * @param password   密码
     * @param code       验证码
     * @param key        唯一标识
     * @param rememberMe 是否记住
     * @return 结果
     */
    String login(String username, String password, String code, String key, boolean rememberMe);


    /**
     * 退出登录
     */
    void logout();
}
