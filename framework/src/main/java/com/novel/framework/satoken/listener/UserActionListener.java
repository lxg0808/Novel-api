package com.novel.framework.satoken.listener;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.listener.SaTokenListener;
import cn.dev33.satoken.stp.SaLoginModel;
import com.novel.framework.enums.OnlineStatus;
import com.novel.framework.redis.ICacheService;
import com.novel.framework.satoken.LoginHelper;
import com.novel.system.domain.LoginUser;
import com.novel.system.domain.SysUserOnline;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.novel.common.constants.Constants.ONLINE_TOKEN_KEY;

/**
 * 用户行为 侦听器的实现
 *
 * @author novel
 * @since 2023/2/7 15:07
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class UserActionListener implements SaTokenListener {

    private final SaTokenConfig tokenConfig;
    private final ICacheService iCacheService;

    /**
     * 每次登录时触发
     */
    @Override
    public void doLogin(String loginType, Object loginId, String tokenValue, SaLoginModel loginModel) {
        LoginUser user = LoginHelper.getLoginUser();
        SysUserOnline sysUserOnline = new SysUserOnline();

        sysUserOnline.setLoginLocation(user.getLoginLocation());
        sysUserOnline.setIpaddr(user.getIpaddr());
        sysUserOnline.setOs(user.getOs());
        sysUserOnline.setBrowser(user.getBrowser());
        sysUserOnline.setLoginName(user.getUser().getUserName());
        if (user.getUser().getDept() != null) {
            sysUserOnline.setDeptName(user.getUser().getDept().getDeptName());
        }
        sysUserOnline.setStatus(OnlineStatus.ON_LINE);
        sysUserOnline.setStartTimestamp(user.getUser().getLoginDate());
        sysUserOnline.setTokenId(tokenValue);
        if (tokenConfig.getTimeout() == -1) {
            iCacheService.set(ONLINE_TOKEN_KEY + tokenValue, sysUserOnline);
        } else {
            iCacheService.set(ONLINE_TOKEN_KEY + tokenValue, sysUserOnline, tokenConfig.getTimeout());
        }
        log.info("user doLogin, userId:{}, token:{}", loginId, tokenValue);
    }

    /**
     * 每次注销时触发
     */
    @Override
    public void doLogout(String loginType, Object loginId, String tokenValue) {
        iCacheService.remove(ONLINE_TOKEN_KEY + tokenValue);
        log.info("user doLogout, userId:{}, token:{}", loginId, tokenValue);
    }

    /**
     * 每次被踢下线时触发
     */
    @Override
    public void doKickout(String loginType, Object loginId, String tokenValue) {
        iCacheService.remove(ONLINE_TOKEN_KEY + tokenValue);
        log.info("user doLogoutByLoginId, userId:{}, token:{}", loginId, tokenValue);
    }

    /**
     * 每次被顶下线时触发
     */
    @Override
    public void doReplaced(String loginType, Object loginId, String tokenValue) {
        iCacheService.remove(ONLINE_TOKEN_KEY + tokenValue);
        log.info("user doReplaced, userId:{}, token:{}", loginId, tokenValue);
    }

    /**
     * 每次被封禁时触发
     */
    @Override
    public void doDisable(String loginType, Object loginId, String service, int level, long disableTime) {
    }

    /**
     * 每次被解封时触发
     */
    @Override
    public void doUntieDisable(String loginType, Object loginId, String service) {
    }

    /**
     * 每次打开二级认证时触发
     */
    @Override
    public void doOpenSafe(String loginType, String tokenValue, String service, long safeTime) {
    }

    /**
     * 每次创建Session时触发
     */
    @Override
    public void doCloseSafe(String loginType, String tokenValue, String service) {
    }

    /**
     * 每次创建Session时触发
     */
    @Override
    public void doCreateSession(String id) {
    }

    /**
     * 每次注销Session时触发
     */
    @Override
    public void doLogoutSession(String id) {
        log.info("user doLogoutSession, sessionId:{}", id);
    }

    /**
     * 每次Token续期时触发
     */
    @Override
    public void doRenewTimeout(String tokenValue, Object loginId, long timeout) {
        log.info("user doRenewTimeout, userId:{}, token:{}", loginId, tokenValue);
    }
}
