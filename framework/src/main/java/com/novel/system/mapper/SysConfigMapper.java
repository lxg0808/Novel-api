package com.novel.system.mapper;

import com.novel.system.domain.SysConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 参数配置 数据层
 *
 * @author novel
 * @since 2020/07/08
 */
@Mapper
public interface SysConfigMapper {

    /**
     * 查询参数配置信息
     *
     * @param id 参数配置ID
     * @return 参数配置信息
     */
    SysConfig selectConfigById(Long id);

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    List<SysConfig> selectConfigList(SysConfig config);

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    int insertConfig(SysConfig config);

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    int updateConfig(SysConfig config);

    /**
     * 删除参数配置
     *
     * @param id 参数配置ID
     * @return 结果
     */
    int deleteConfigById(Long id);

    /**
     * 批量删除参数配置
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int batchDeleteConfig(Long[] ids);

    /**
     * 查看key是否唯一
     *
     * @param configKey key
     * @return 结果
     */
    SysConfig checkConfigKeyUnique(String configKey);
}
