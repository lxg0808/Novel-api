package com.novel.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.satoken.LoginHelper;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysPost;
import com.novel.system.service.SysPostService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 岗位信息操作处理
 *
 * @author novel
 * @since 2019/6/11
 */
@RestController
@RequestMapping("/system/post")
public class SysPostController extends BaseController {
    private final SysPostService postService;

    public SysPostController(SysPostService postService) {
        this.postService = postService;
    }

    /**
     * 获取岗位列表
     *
     * @param post 岗位查询条件
     * @return 岗位列表
     */
    @SaCheckPermission(value = {"system:post:list", "system:user:edit", "system:user:add"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo list(SysPost post) {
        startPage();
        List<SysPost> list = postService.selectPostList(post);
        return getDataTable(list);
    }

    /**
     * 删除岗位信息
     *
     * @param ids 岗位id数组
     * @return 删除结果
     */
    @SaCheckPermission("system:post:remove")
    @Log(title = "岗位管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/remove")
    public Result remove(String ids) {
        return toAjax(postService.deletePostByIds(ids));
    }

    /**
     * 新增保存岗位
     *
     * @param post 岗位信息
     * @return 新增结果
     */
    @SaCheckPermission("system:post:add")
    @Log(title = "岗位管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) SysPost post) {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        } else if (UserConstants.POST_CODE_NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        post.setCreateBy(LoginHelper.getUserName());
        return toAjax(postService.insertPost(post));
    }


    /**
     * 修改保存岗位
     *
     * @param post 岗位信息
     * @return 修改结果
     */
    @SaCheckPermission("system:post:edit")
    @Log(title = "岗位管理", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) SysPost post) {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        } else if (UserConstants.POST_CODE_NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        post.setUpdateBy(LoginHelper.getUserName());
        return toAjax(postService.updatePost(post));
    }

    /**
     * 校验岗位名称
     *
     * @param post 岗位信息（带岗位名称）
     * @return 校验结果
     */
    @PostMapping("/checkPostNameUnique")
    public Result checkPostNameUnique(SysPost post) {
        return toAjax(postService.checkPostNameUnique(post));
    }

    /**
     * 校验岗位编码
     *
     * @param post 岗位信息（带岗位编码）
     * @return 校验结果
     */
    @PostMapping("/checkPostCodeUnique")
    public Result checkPostCodeUnique(SysPost post) {
        return toAjax(postService.checkPostCodeUnique(post));
    }

    /**
     * 导出岗位信息
     *
     * @param post 岗位查询条件
     * @return 导出结果
     */
    @Log(title = "岗位管理", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:post:export")
    @GetMapping("/export")
    public Result export(SysPost post) {
        startPage();
        List<SysPost> postList = postService.selectPostList(post);
        String fileName = ExcelUtils.exportExcelToFile(postList, "岗位管理", SysPost.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }
}
