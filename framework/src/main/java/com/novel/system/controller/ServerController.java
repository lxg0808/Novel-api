package com.novel.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.novel.common.cache.service.ICacheService;
import com.novel.framework.base.BaseController;
import com.novel.framework.result.Result;
import com.novel.framework.web.domain.Server;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author novel
 * @since 2019/5/21
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {

    private final ICacheService cacheService;

    public ServerController(ICacheService cacheService) {
        this.cacheService = cacheService;
    }

    /**
     * 获取服务器运行信息
     *
     * @return 服务器运行信息
     */
    @SaCheckPermission("monitor:server:list")
    @GetMapping()
    public Result server() {
        Server server = new Server();
        server.copyTo();
        server.setCache(cacheService.getCacheInfo());
        return toAjax(server);
    }

    /**
     * 模拟nginx信息  开发环境下使用
     *
     * @return 服务器运行信息
     */
    @SaCheckPermission("monitor:server:list")
    @GetMapping("/nginx")
    public Result nginx() {
        return toAjax("Active connections: 0 \n" +
                "server accepts handled requests\n" +
                " 0 0 0 \n" +
                "Reading: 0 Writing: 0 Waiting: 0 ");
    }
}
