package com.novel.system.domain;

import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serial;
import java.util.List;

/**
 * 部门表 sys_dept
 *
 * @author novel
 * @since 2019/5/15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class SysDept extends BaseModel {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 父部门ID
     */
    @Range(min = 0, message = "父级部门选择不正确", groups = {AddGroup.class, EditGroup.class})
    private Long parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空",  groups = {AddGroup.class, EditGroup.class})
    @Size(max = 10, message = "部门名称长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    private String deptName;

    /**
     * 显示顺序
     */
    @NotBlank(message = "显示顺序不能为空",  groups = {AddGroup.class, EditGroup.class})
    @Range(min = 0, message = "显示顺序不正确", groups = {AddGroup.class, EditGroup.class})
    private String orderNum;

    /**
     * 负责人
     */
    @NotBlank(message = "负责人名称不能为空",  groups = {AddGroup.class, EditGroup.class})
    @Size(max = 10, message = "负责人名称长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    private String leader;

    /**
     * 联系电话
     */
    @NotBlank(message = "负责人电话不能为空",  groups = {AddGroup.class, EditGroup.class})
    @Size(max = 11, message = "联系电话长度不能超过11个字符", groups = {AddGroup.class, EditGroup.class})
    private String phone;

    /**
     * 邮箱
     */
    @Email(message = "邮箱格式不正确", groups = {AddGroup.class, EditGroup.class})
    @Size(max = 50, message = "邮箱长度不能超过50个字符", groups = {AddGroup.class, EditGroup.class})
    private String email;

    /**
     * 部门状态:0正常,1停用
     */
    @NotBlank(message = "部门状态不能为空",  groups = {AddGroup.class, EditGroup.class})
    @Pattern(regexp = "^0|1$", message = "部门状态错误", groups = {AddGroup.class, EditGroup.class})
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 父部门名称
     */
    private String parentName;
    /**
     * 子部门
     */
    private List<SysDept> children;
}
