package com.novel.system.domain;

import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serial;

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author novel
 * @since 2019/4/16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysRoleMenu extends BaseModel {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;
}
