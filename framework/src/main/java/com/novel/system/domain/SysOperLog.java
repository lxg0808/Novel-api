package com.novel.system.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serial;
import java.util.Date;

/**
 * 操作日志记录表 oper_log
 *
 * @author novel
 * @since 2019/5/14
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class SysOperLog extends BaseModel {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 操作模块
     */
    @Excel(name = "操作模块")
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    @Excel(name = "业务类型", replace = {"其他_0", "新增_1", "修改_2", "删除_3", "授权_4", "导出_5", "导入_6", "强退_7", "生成代码_8", "清空数据_9"})
    private Integer businessType;

    /**
     * 请求方法
     */
    @Excel(name = "请求方法")
    private String method;
    /**
     * 请求方式
     */
    @Excel(name = "请求方式")
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @Excel(name = "操作类别", replace = {"其他_0", "后台用户_1", "手机端用户_2"})
    private Integer operatorType;

    /**
     * 操作人员
     */
    @Excel(name = "操作人员")
    private String operName;

    /**
     * 部门名称
     */
    @Excel(name = "部门名称")
    private String deptName;

    /**
     * 请求url
     */
    @Excel(name = "请求地址")
    private String operUrl;

    /**
     * 操作地址
     */
    @Excel(name = "操作地址")
    private String operIp;

    /**
     * 操作地点
     */
    @Excel(name = "操作地点")
    private String operLocation;

    /**
     * 请求参数
     */
    @Excel(name = "请求参数")
    private String operParam;

    /**
     * 返回参数
     */
    @Excel(name = "返回参数")
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @Excel(name = "操作状态", replace = {"正常_0", "异常_1"})
    private Integer status;
    /**
     * 处理耗时
     */
    @Excel(name = "处理耗时", suffix = "ms")
    private Long processingTime;
    /**
     * 错误消息
     */
    @Excel(name = "错误消息")
    private String errorMsg;

    /**
     * 操作时间
     */
    @Excel(name = "操作时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;
}
