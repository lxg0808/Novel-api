package com.novel.system.domain.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 树组件对应结构体
 *
 * @author novel
 * @since 2019/4/30
 */
@Data
@ToString
public class TreeData implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 菜单id
     */
    private Long id;
    /**
     * 菜单名称
     */
    private String label;
    /**
     * 菜单显示顺序
     */
    private String orderNum;
    /**
     * 子节点
     */
    private List<TreeData> children;
}
