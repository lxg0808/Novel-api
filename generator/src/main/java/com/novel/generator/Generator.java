package com.novel.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 代码生成器
 *
 * @author novel
 * @since 2023/8/3 16:00
 */
@Slf4j
@Component
public class Generator {

    // 获取当前项目的端口
    @Value("${server.port}")
    private Integer port;

    @EventListener(ApplicationReadyEvent.class)
    @Async
    public void print() {
        log.debug("代码生成器访问地址：http://localhost:{}/maku-generator/index.html", port);
    }

}
