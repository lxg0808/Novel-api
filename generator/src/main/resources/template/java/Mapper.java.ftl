package ${package}.${moduleName}.mapper;

import ${package}.${moduleName}.domain.${ClassName};
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
* ${tableComment} 数据层
*
* @author ${author}
* @since  ${datetime}
*/
@Mapper
public interface ${ClassName}Mapper {
    /**
     * 根据id查询
     *
     * @param ${primaryList[0].fieldName} ${primaryList[0].fieldComment}
     * @return ${tableComment}信息
     */
    ${ClassName} select${ClassName}ById(${primaryList[0].attrType} ${primaryList[0].fieldName});

    /**
     * 根据条件分页查询${tableComment}
     *
     * @param ${className} ${tableComment}
     * @return ${tableComment}集合信息
     */
    List<${ClassName}> select${ClassName}List(${ClassName} ${className});

    /**
     * 修改${tableComment}
     *
     * @param ${className} ${tableComment}
     * @return 影响的行数
     */
    int update${ClassName}ById(${ClassName} ${className});

    /**
     * 新增${tableComment}
     *
     * @param ${className} ${tableComment}
     * @return 影响的行数
     */
    int insert${ClassName}(${ClassName} ${className});

    /**
     * 根据主键删除${tableComment}
     *
     * @param ${primaryList[0].fieldName} ${primaryList[0].fieldComment}
     * @return 影响的行数
     */
    int delete${ClassName}ById(${primaryList[0].attrType} ${primaryList[0].fieldName});

    /**
     * 批量删除${tableComment}
     *
     * @param ${primaryList[0].fieldName}s 需要删除的${primaryList[0].fieldComment}集合
     * @return 影响的行数
     */
    int batchDelete${ClassName}ByIds(${primaryList[0].attrType}[] ${primaryList[0].fieldName}s);
}