package ${package}.${moduleName}.service.impl;

import lombok.AllArgsConstructor;
import java.util.List;
import org.springframework.stereotype.Service;
import com.novel.common.utils.StringUtils;
import ${package}.${moduleName}.mapper.${ClassName}Mapper;
import ${package}.${moduleName}.domain.${ClassName};
import ${package}.${moduleName}.service.I${ClassName}Service;

/**
 * ${tableComment} 服务层实现
 *
 * @author ${author}
 * @since ${datetime}
 */
@Service
@AllArgsConstructor
public class ${ClassName}ServiceImpl implements I${ClassName}Service {
    private final ${ClassName}Mapper ${className}Mapper;

    /**
     * 查询${functionName}信息
     *
     * @param ${primaryList[0].fieldName} ${primaryList[0].fieldComment}
     * @return ${functionName}信息
     */
    @Override
    public ${ClassName} select${ClassName}ById(${primaryList[0].attrType} ${primaryList[0].fieldName}) {
        return ${className}Mapper.select${ClassName}ById(${primaryList[0].fieldName});
    }

    /**
     * 查询${functionName}列表
     *
     * @param ${className} ${functionName}信息
     * @return ${functionName}集合
     */
    @Override
    public List<${ClassName}> select${ClassName}List(${ClassName} ${className}) {
        return ${className}Mapper.select${ClassName}List(${className});
    }

    /**
     * 新增${functionName}
     *
     * @param ${className} ${functionName}信息
     * @return 结果
     */
    @Override
    public boolean insert${ClassName}(${ClassName} ${className}) {
        return ${className}Mapper.insert${ClassName}(${className}) > 0;
    }

    /**
     * 修改${functionName}
     *
     * @param ${className} ${functionName}信息
     * @return 结果
     */
    @Override
    public boolean update${ClassName}ById(${ClassName} ${className}) {
        return ${className}Mapper.update${ClassName}ById(${className}) > 0;
    }

    /**
     * 保存${functionName}
     *
     * @param ${className} ${functionName}信息
     * @return 结果
     */
    @Override
    public boolean save${ClassName}(${ClassName} ${className}) {
        ${primaryList[0].attrType} ${primaryList[0].attrName} = ${className}.get${primaryList[0].attrName?cap_first}();
        int rows;
        if (StringUtils.isNotNull(${primaryList[0].attrName})) {
            rows = ${className}Mapper.update${ClassName}ById(${className});
        } else {
            rows = ${className}Mapper.insert${ClassName}(${className});
        }
        return rows > 0;
    }

    /**
     * 删除${functionName}信息
     *
     * @param ${primaryList[0].fieldName} ${primaryList[0].fieldComment}
     * @return 结果
     */
    @Override
    public boolean delete${ClassName}ById(${primaryList[0].attrType} ${primaryList[0].attrName}) {
        return ${className}Mapper.delete${ClassName}ById(${primaryList[0].attrName}) > 0;
    }

    /**
     * 批量删除${functionName}信息
     *
     * @param ${primaryList[0].fieldName}s 需要删除的${primaryList[0].fieldComment}集合
     * @return 结果
     */
    @Override
    public boolean batchDelete${ClassName}(${primaryList[0].attrType}[] ${primaryList[0].attrName}s) {
        return ${className}Mapper.batchDelete${ClassName}ByIds(${primaryList[0].attrName}s) > 0;
    }
}