package ${package}.${moduleName}.controller;

import lombok.AllArgsConstructor;
import com.novel.framework.annotation.Log;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ${package}.${moduleName}.domain.${ClassName};
import ${package}.${moduleName}.service.I${ClassName}Service;
import com.novel.framework.base.BaseController;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.framework.satoken.LoginHelper;

import java.util.Date;

/**
 * ${tableComment} 控制器
 *
 * @author ${author}
 * @since ${datetime}
 */
@RestController
@RequestMapping("/${moduleName}/${functionName}")
@AllArgsConstructor
public class ${ClassName}Controller extends BaseController {
    private final I${ClassName}Service ${className}Service;


    /**
     * 查询${tableComment}列表
     */
    @SaCheckPermission("${moduleName}:${functionName}:list")
    @GetMapping("/list")
    public TableDataInfo list(${ClassName} ${className}){
        startPage();
        return getDataTable(${className}Service.select${ClassName}List(${className}));
    }

    /**
     * 新增${tableComment}
     *
     * @param ${className} ${tableComment}
     * @return 操作结果
     */
    @Log(title = "${tableComment}", businessType = BusinessType.INSERT)
    @SaCheckPermission("${moduleName}:${functionName}:add")
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) ${ClassName} ${className}) {
        ${className}.setCreateBy(LoginHelper.getUserName());
        ${className}.setCreateTime(new Date());
        return toAjax(${className}Service.insert${ClassName}(${className}), "新增${tableComment}成功", "新增${tableComment}失败");
    }

    /**
     * 保存编辑${tableComment}
     *
     * @param ${className} ${tableComment}
     * @return 操作结果
     */
    @Log(title = "${tableComment}", businessType = BusinessType.UPDATE)
    @SaCheckPermission("${moduleName}:${functionName}:edit")
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) ${ClassName} ${className}) {
        ${className}.setUpdateBy(LoginHelper.getUserName());
        ${className}.setUpdateTime(new Date());
        return toAjax(${className}Service.update${ClassName}ById(${className}), "修改${tableComment}成功", "修改${tableComment}失败");
    }

    /**
     * 删除${tableComment}
     *
     * @param ${primaryList[0].fieldName}s 需要删除的${primaryList[0].fieldComment}集合
     * @return 操作结果
     */
    @Log(title = "${tableComment}", businessType = BusinessType.DELETE)
    @SaCheckPermission("${moduleName}:${functionName}:remove")
    @DeleteMapping("/remove")
    public Result remove(${primaryList[0].attrType}[] ${primaryList[0].fieldName}s) {
        return toAjax(${className}Service.batchDelete${ClassName}(${primaryList[0].fieldName}s), "删除${tableComment}成功", "删除${tableComment}失败");
    }
}