package com.novel.captcha.exception;

/**
 * 验证码为空异常
 *
 * @author novel
 * @since 2023/8/3 18:12
 */
public class CaptchaIsEmptyException extends CaptchaException {
    public CaptchaIsEmptyException() {
        super("Captcha is  Empty");
    }

    public CaptchaIsEmptyException(String message) {
        super(message);
    }
}
