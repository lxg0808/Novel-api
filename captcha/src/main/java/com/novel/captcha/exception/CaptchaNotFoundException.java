package com.novel.captcha.exception;

/**
 * 验证码不存在
 *
 * @author novel
 * @since 2023/8/3 18:12
 */
public class CaptchaNotFoundException extends CaptchaException {
    public CaptchaNotFoundException() {
        super("Captcha Not Found");
    }

    public CaptchaNotFoundException(String message) {
        super(message);
    }
}
