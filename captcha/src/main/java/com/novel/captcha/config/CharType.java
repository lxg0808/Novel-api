package com.novel.captcha.config;

import com.wf.captcha.base.Captcha;
import lombok.Getter;
/**
 * 验证码字符类型
 *
 * @author novel
 * @since 2023/8/3 18:10
 */
@Getter
public enum CharType {
    /**
     * 数字和字母混合
     */
    TYPE_DEFAULT(Captcha.TYPE_DEFAULT),
    /**
     * 纯数字
     */
    TYPE_ONLY_NUMBER(Captcha.TYPE_ONLY_NUMBER),
    /**
     * 纯字母
     */
    TYPE_ONLY_CHAR(Captcha.TYPE_ONLY_CHAR),
    /**
     * 纯大写字母
     */
    TYPE_ONLY_UPPER(Captcha.TYPE_ONLY_UPPER),
    /**
     * 纯小写字母
     */
    TYPE_ONLY_LOWER(Captcha.TYPE_ONLY_LOWER),
    /**
     * 数字和大写字母
     */
    TYPE_NUM_AND_UPPER(Captcha.TYPE_NUM_AND_UPPER);

    private final int type;

    CharType(int type) {
        this.type = type;
    }
}
