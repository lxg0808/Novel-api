package com.novel.captcha.config;

import com.wf.captcha.base.Captcha;
import lombok.Getter;

/**
 * 验证码字体类型
 *
 * @author novel
 * @since 2023/8/3 18:11
 */
@Getter
public enum Font {

    FONT_1(Captcha.FONT_1),
    FONT_2(Captcha.FONT_2),
    FONT_3(Captcha.FONT_3),
    FONT_4(Captcha.FONT_4),
    FONT_5(Captcha.FONT_5),
    FONT_6(Captcha.FONT_6),
    FONT_7(Captcha.FONT_7),
    FONT_8(Captcha.FONT_8),
    FONT_9(Captcha.FONT_9),
    FONT_10(Captcha.FONT_10);


    private final int type;

    Font(int type) {
        this.type = type;
    }

}