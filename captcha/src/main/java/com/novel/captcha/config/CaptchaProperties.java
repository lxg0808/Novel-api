package com.novel.captcha.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 验证码属性配置
 *
 * @author novel
 * @since 2023/8/3 18:10
 */
@ConfigurationProperties(prefix = CaptchaProperties.PREFIX)
@Data
public class CaptchaProperties {
    public static final String PREFIX = "captcha";
    /**
     * 验证码字符类型
     */
    private CharType type = CharType.TYPE_DEFAULT;
    /**
     * 字体
     */
    private Font font = Font.FONT_1;
    /**
     * 验证码类型
     */
    private CaptchaType captchaType = CaptchaType.PNG;
    /**
     * 宽度
     */
    private Integer width = 130;
    /**
     * 高度
     */
    private Integer height = 38;
    /**
     * 验证码长度
     */
    private Integer length = 4;

    /**
     * 是否区分大小写
     */
    private boolean caseSensitivity = false;
}





