package com.novel.captcha.cache;

import com.novel.captcha.cache.config.CacheType;

/**
 * 保存验证码接口
 *
 * @author novel
 * @since 2023/8/3 18:06
 */
public interface CaptchaCache {
    /**
     * 初始化缓存对象
     */
    void init();

    /**
     * 销毁缓存对象
     */
    void destroy();

    /**
     * 获取缓存类型
     *
     * @return 缓存类型
     */
    CacheType getCacheType();

    /**
     * 保存验证码
     *
     * @param key  验证码key
     * @param code 验证码
     */
    void saveCaptcha(String key, String code);

    /**
     * 获取验证码
     *
     * @param key 验证码key
     * @return 验证码
     */
    String getCaptcha(String key);

    /**
     * 删除验证码
     *
     * @param key 验证码key
     */
    void removeCaptcha(String key);
}
