package com.novel.captcha.cache.manager;

import com.novel.captcha.cache.CaptchaCache;
import com.novel.captcha.cache.impl.SessionCaptchaCache;
import com.novel.captcha.exception.CaptchaCacheException;

/**
 * 默认缓存管理器
 *
 * @author novel
 * @since 2023/8/3 18:10
 */
public class SessionCaptchaCacheManagerImpl extends AbstractCaptchaCacheManager {

    private final SessionCaptchaCache sessionCaptchaCache;

    public SessionCaptchaCacheManagerImpl(SessionCaptchaCache sessionCaptchaCache) {
        this.sessionCaptchaCache = sessionCaptchaCache;
    }

    @Override
    CaptchaCache createCache(String key) throws CaptchaCacheException {
        sessionCaptchaCache.setPrefix(key);
        return sessionCaptchaCache;
    }
}
