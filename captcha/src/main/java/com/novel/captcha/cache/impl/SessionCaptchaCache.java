package com.novel.captcha.cache.impl;


import com.novel.captcha.cache.AbstractCaptchaCache;
import com.novel.captcha.cache.config.CaptchaCacheProperties;
import com.novel.captcha.exception.CaptchaTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * session 存储实现captcha缓存
 *
 * @author novel
 * @since 2023/8/3 18:08
 */
@Slf4j
public class SessionCaptchaCache extends AbstractCaptchaCache {
    private static final String CAPTCHA_SESSION_DATE = "CAPTCHA_SESSION_DATE";
    @Autowired
    private HttpServletRequest request;

    public SessionCaptchaCache(CaptchaCacheProperties captchaCacheProperties) {
        super(captchaCacheProperties);
    }

    @Override
    public void saveCaptcha(String key, String code) {
        HttpSession session = request.getSession();
        session.setAttribute(getPrefix() + ":" + key, code);
        session.setAttribute(CAPTCHA_SESSION_DATE, System.currentTimeMillis() + getCaptchaCacheProperties().getCacheTimeOut());
    }

    @Override
    public String getCaptcha(String key) {
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(CAPTCHA_SESSION_DATE);
        if (!ObjectUtils.isEmpty(attribute)) {
            Long timeout = (Long) attribute;
            if (timeout >= System.currentTimeMillis()) {
                return (String) session.getAttribute(getPrefix() + ":" + key);
            } else {
                throw new CaptchaTimeoutException();
            }
        } else {
            throw new CaptchaTimeoutException();
        }
    }

    @Override
    public void removeCaptcha(String key) {
        HttpSession session = request.getSession();
        session.removeAttribute(getPrefix() + ":" + key);
    }
}
