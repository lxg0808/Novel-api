package com.novel.captcha.adapter.impl;

import com.novel.captcha.adapter.AbstractCaptchaImpl;
import com.novel.captcha.cache.manager.CaptchaCacheManager;
import com.novel.captcha.config.CaptchaProperties;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;

/**
 * 字母png类型验证码实现类
 *
 * @author novel
 * @since 2019/12/2
 */
public class SpecCaptchaImpl extends AbstractCaptchaImpl {

    public SpecCaptchaImpl(CaptchaCacheManager captchaCacheManager, CaptchaProperties captchaProperties) {
        super(captchaCacheManager, captchaProperties);
    }


    @Override
    public Captcha getCaptcha() {
        return new SpecCaptcha();
    }
}
