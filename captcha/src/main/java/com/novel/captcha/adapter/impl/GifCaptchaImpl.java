package com.novel.captcha.adapter.impl;

import com.novel.captcha.adapter.AbstractCaptchaImpl;
import com.novel.captcha.cache.manager.CaptchaCacheManager;
import com.novel.captcha.config.CaptchaProperties;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.base.Captcha;

/**
 * 字母png类型验证码实现类
 *
 * @author novel
 * @since 2019/12/2
 */
public class GifCaptchaImpl extends AbstractCaptchaImpl {
    public GifCaptchaImpl(CaptchaCacheManager captchaCacheManager, CaptchaProperties captchaProperties) {
        super(captchaCacheManager, captchaProperties);
    }

    @Override
    public Captcha getCaptcha() {
        return new GifCaptcha();
    }
}
