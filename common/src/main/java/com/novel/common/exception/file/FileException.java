package com.novel.common.exception.file;

import com.novel.common.exception.base.BaseException;

import java.io.Serial;

/**
 * 文件信息异常类
 *
 * @author novel
 * @since 2019/5/24
 */
public class FileException extends BaseException {
    @Serial
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args) {
        super("file", code, args, null);
    }

    public FileException(String module, String code, Object[] args, String defaultMessage) {
        super(module, code, args, defaultMessage);
    }

    public FileException(String module, String code, Object[] args) {
        super(module, code, args);
    }

    public FileException(String module, String defaultMessage) {
        super(module, defaultMessage);
    }

    public FileException(String defaultMessage) {
        super(defaultMessage);
    }
}
