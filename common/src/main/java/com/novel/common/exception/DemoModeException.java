package com.novel.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 演示模式异常
 *
 * @author novel
 * @since 2019/12/5
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DemoModeException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;
    private String msg;

    public DemoModeException() {
    }

    public DemoModeException(String msg) {
        super(msg);
        setMsg(msg);
    }
}
