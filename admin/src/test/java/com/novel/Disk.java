package com.novel;

import cn.dev33.satoken.secure.BCrypt;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

/**
 * @author novel
 * @since 2022/4/23 0:08
 */
public class Disk {
    public static void main(String[] args) {
     /*   SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        hal.getDiskStores().forEach(d -> {
            System.out.println("Disk Name: " + d);
        });*/
//$2a$10$XV3dhBVIi.Iv6ciOPy1vCuULId8DbFLbCpAUrKMTETRb8BrTrrG.C

        System.out.println(BCrypt.hashpw("123456"));
        System.out.println(BCrypt.checkpw("123456","$2a$10$XV3dhBVIi.Iv6ciOPy1vCuULId8DbFLbCpAUrKMTETRb8BrTrrG.C"));
    }
}
